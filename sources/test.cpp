#include <iostream>

#include <boost/timer/timer.hpp>

#include "All"

int main()
{
    Eigen::Vector3d point1, point2;
    point1.fill(2);
    point2.fill(-1);
    FilterPlane<double> fp(0);
    FilterPlane<double> fp2(3);

    auto fn = MakeFilterNot<double>(fp);
    auto fa = MakeFilterAnd<double>(fp, fp2);
    (void) fn; (void) fa; // unused in release

    assert(fp.Filter(point1) == true && "point above plane");
    assert(fp.Filter(point2) == false && "point below plane");

    assert(fn.Filter(point1) == false && "point above plane");
    assert(fn.Filter(point2) == true && "point below plane");

    assert(fa.Filter(point1) == false && "point1 and true");
    assert(fa.Filter(point2) == false && "point2 and false");

    Eigen::MatrixXd mat(5, 10000);

    for(int i = 0; i < mat.rows(); ++i)
    {
        for(int j = 0; j < mat.cols(); ++j)
        {
            mat(i,j) = j;
        }
    }

    FilterPlane<double> fp3(100);
    FilterArray expected(mat.cols());
    expected.fill(false);
    expected.segment(101,mat.cols()-101).fill(true);

    FilterArray result = Apply<double,FilterPlane<double>>(mat, fp3);

    for(int i = 0; i < result.size(); ++i)
    {
        assert(result(i) == expected(i) && "Not expected result");
    }

    // Let us benchmark our solutions:
    Eigen::MatrixXd mat2(5, 1000000);

    for(int i = 0; i < mat2.rows(); ++i)
    {
        for(int j = 0; j < mat2.cols(); ++j)
        {
            mat2(i,j) = j;
        }
    }

    // we create a 6 layers filter:
    FilterSphere<double> fs0(0), fs1(1), fs2(2), fs3(3), fs4(4), fs5(5);
    DynSphereFilter dfs0(0), dfs1(1), dfs2(2), dfs3(3), dfs4(4), dfs5(5);
    FullDynSphereFilter fdfs0(0), fdfs1(1), fdfs2(2), fdfs3(3), fdfs4(4), fdfs5(5);

    auto fab = MakeFilterAnd<double>(fs0, fs1, fs2, fs3, fs4, fs5);

    DynAndFilter dfab;
    dfab.AddFilter(&dfs0);
    dfab.AddFilter(&dfs1);
    dfab.AddFilter(&dfs2);
    dfab.AddFilter(&dfs3);
    dfab.AddFilter(&dfs4);
    dfab.AddFilter(&dfs5);

    FullDynAndFilter fdfab;
    fdfab.AddFilter(&fdfs0);
    fdfab.AddFilter(&fdfs1);
    fdfab.AddFilter(&fdfs2);
    fdfab.AddFilter(&fdfs3);
    fdfab.AddFilter(&fdfs4);
    fdfab.AddFilter(&fdfs5);

    FilterArray result_true = DynApply(mat2, &dfab);
    {
        boost::timer::auto_cpu_timer t;
        for(int i = 0; i < 1000; ++i)
        {
            FilterArray result2 = DynApply(mat2, &dfab);
            for(int i = 0; i < result_true.size(); ++i)
            {
                if(result_true(i) != result2(i))
                {
                    std::cout << "issue with result at: " << i << std::endl;
                }
            }

        }
        std::cout << "dynamic polymorphism ... " << std::endl;
    }

    {
        boost::timer::auto_cpu_timer t;
        for(int i = 0; i < 1000; ++i)
        {
            //Apply<double, FilterAnd<double, FilterSphere<double>, FilterSphere<double>, FilterSphere<double>, FilterSphere<double>, FilterSphere<double>, FilterSphere<double>>>(mat2, fab);
            FilterArray result2 = Apply<double>(mat2, fab);
            for(int i = 0; i < result_true.size(); ++i)
            {
                if(result_true(i) != result2(i))
                {
                    std::cout << "issue with result at: " << i << std::endl;
                }
            }
        }
        std::cout << "static polymorphism ... " << std::endl;
    }

    {
        boost::timer::auto_cpu_timer t;
        for(int i = 0; i < 1000; ++i)
        {
            //Apply<double, FilterAnd<double, FilterSphere<double>, FilterSphere<double>, FilterSphere<double>, FilterSphere<double>, FilterSphere<double>, FilterSphere<double>>>(mat2, fab);
            FilterArray result2 = fdfab.Filter(mat2);
            for(int i = 0; i < result_true.size(); ++i)
            {
                if(result_true(i) != result2(i))
                {
                    std::cout << "issue with result at: " << i << std::endl;
                }
            }
        }
        std::cout << "dynamic on matrix ... " << std::endl;
    }

    return 0;
}
