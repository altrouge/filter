Project to test various implementations of a filter that checks whether a point fulfill a condition.

It can be used to filter points based on geometrical forms (remove close points, points in a special area ...).

The goal is also to evaluate the speed-up gain using static polymorphism.

It compiles (and is tested) on:
- Clang
- g++5

It requires the installation of:
- Eigen3

A docker to test it can be downloaded at https://hub.docker.com/r/djartoux/icp/
