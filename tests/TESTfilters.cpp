#define BOOST_TEST_MODULE Filters

#include <boost/test/unit_test.hpp>
#include <boost/timer/timer.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include "All"

BOOST_AUTO_TEST_SUITE(filters)

BOOST_AUTO_TEST_CASE(PlaneFilter)
{
    Eigen::Vector3d point1, point2, point3;

    point1.fill(2);
    point2.fill(-1);
    point3.fill(0);
    auto fp = FilterPlane<double>(0.);

    BOOST_CHECK_EQUAL(fp.Filter(point1), true);
    BOOST_CHECK_EQUAL(fp.Filter(point2), false);
    BOOST_CHECK_EQUAL(fp.Filter(point3), false);
}

BOOST_AUTO_TEST_CASE(NotFilter)
{
    Eigen::Vector3d point1, point2, point3;

    point1.fill(2);
    point2.fill(-1);
    point3.fill(-1);
    FilterPlane<double> fp(0);
    //auto fn = FilterNot<double, FilterPlane<double>>(fp);
    auto fn = MakeFilterNot<double>(fp);

    BOOST_CHECK_EQUAL(fn.Filter(point1), false);
    BOOST_CHECK_EQUAL(fn.Filter(point2), true);
    BOOST_CHECK_EQUAL(fn.Filter(point3), true);
}

BOOST_AUTO_TEST_CASE(AndFilter)
{
    Eigen::Vector3d point1, point2, point3;

    point1.fill(2);
    point2.fill(0);
    point3.fill(4);
    FilterPlane<double> fp(0), fp2(3);
    auto fa = MakeFilterAnd<double>(fp, fp2);
    auto fa2 = MakeFilterAnd<double>(fp2, fp);
    //auto fa = FilterAnd<double, FilterPlane<double>, FilterPlane<double>>(fp, fp2);

    // fp true, fp2 false: false
    BOOST_CHECK_EQUAL(fa.Filter(point1), false);
    // fp true, fp2 true: true
    BOOST_CHECK_EQUAL(fa.Filter(point3), true);
    // fp false, fp2 true: false (with switch fp fp2)
    BOOST_CHECK_EQUAL(fa2.Filter(point1), false);
    // fp false, fp2 false: false
    BOOST_CHECK_EQUAL(fa.Filter(point2), false);
}

BOOST_AUTO_TEST_CASE(OrFilter)
{
    Eigen::Vector3d point1, point2, point3;

    point1.fill(2);
    point2.fill(0);
    point3.fill(4);
    FilterPlane<double> fp(0), fp2(3);
    auto fa = MakeFilterOr<double>(fp, fp2);
    auto fa2 = MakeFilterOr<double>(fp2, fp);
    //auto fa = FilterAnd<double, FilterPlane<double>, FilterPlane<double>>(fp, fp2);

    // fp true, fp2 false: true
    BOOST_CHECK_EQUAL(fa.Filter(point1), true);
    // fp true, fp2 true: true
    BOOST_CHECK_EQUAL(fa.Filter(point3), true);
    // fp false, fp2 true: true (with switch fp fp2)
    BOOST_CHECK_EQUAL(fa2.Filter(point1), true);
    // fp false, fp2 false: false
    BOOST_CHECK_EQUAL(fa.Filter(point2), false);
}

BOOST_AUTO_TEST_SUITE_END()
