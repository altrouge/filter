#ifndef _FILTER_COMMON_H_
#define _FILTER_COMMON_H_

#include <Eigen/Core>
#include <Eigen/Geometry>

using FilterArray = Eigen::Array<bool, 1, Eigen::Dynamic>;

#endif
