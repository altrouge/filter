#ifndef _CRTPFILTER_H_
#define _CRTPFILTER_H_

#include <tuple>

#include "Eigen/Core"
#include "Eigen/Geometry"

#include "common.h"

/*! Base class for filter CRTP */
template<typename TScalar, class Derived> class FilterBase
{
    public:
    using Scalar = TScalar; ///< Scalar defined to used scalar type

    /*! Check a condition on a point.
     * \param m Point (1D) used.
     * \return Whether the point respects a set condition.
     */
    inline bool Filter(const Eigen::Ref<const Eigen::Matrix<TScalar, 1, Eigen::Dynamic>>& m)
    {
        return static_cast<Derived*>(this)->FilterImpl(m);
    }
};

/*! Filter points above a plane.
 */
template<typename Scalar> class FilterPlane: public FilterBase<Scalar, FilterPlane<Scalar>>
{
    public:

    /*! Default constructor
     * \param z_min min heigth, defaults to 0.
     */
    FilterPlane(Scalar z_min = 0): m_z_min(z_min)
    {
    }

    /*! Filter implementation
     */
    bool FilterImpl(const Eigen::Ref<const Eigen::Matrix<Scalar, 1, Eigen::Dynamic>>& m)
    {
        assert(m.size() >= 3 && "Not enough values in vector");

        return (m(2) > m_z_min);
    }

    Scalar m_z_min; ///< Min heigth, if point greater, evaluates to 0
};

/*! Filter points in a sphere.
 */
template<typename Scalar> class FilterSphere: public FilterBase<Scalar, FilterSphere<Scalar>>
{
    public:
    /*! Default constructor
     * \param radius Check if the point is inside a sphere of this radius (origin 0).
     */
    FilterSphere(Scalar radius = 0): m_radius2(radius*radius)
    {
    }

    /*! Filter implementation
     */
    bool FilterImpl(const Eigen::Ref<const Eigen::Matrix<Scalar, 1, Eigen::Dynamic>>& m)
    {
        assert(m.size() >= 3 && "Not enough values in vector");

        return (m(0) * m(0) + m(1) * m(1) + m(2) * m(2) < m_radius2);
    }

    Scalar m_radius2; ///< Squared radius, filter true if squared distance to point lower than this.
};

/*! Filter points in a cylinder.
 */
template<typename Scalar> class FilterCylinder: public FilterBase<Scalar, FilterCylinder<Scalar>>
{
    public:
    /*! Default constructor
     * \param radius Cylinder radius.
     * \param z_min Cylinder minimum height.
     * \param z_max Cylinder maximum height.
     */
    FilterCylinder(Scalar radius = 0, Scalar z_min = 0, Scalar z_max = 0):
        m_radius2(radius*radius),
        m_z_min(z_min),
        m_z_max(z_max)
    {
    }

    /*! Filter implementation
     */
    bool FilterImpl(const Eigen::Ref<const Eigen::Matrix<Scalar, 1, Eigen::Dynamic>>& m)
    {
        assert(m.size() >= 3 && "Not enough values in vector");

        return (m(2) > m_z_min && m(2) < m_z_max && m(0)*m(0) + m(1)*m(1) < m_radius2);
    }

    Scalar m_radius2; ///< Squared radius.
    Scalar m_z_min; ///< Min height.
    Scalar m_z_max; ///< Max height.
};

/*! Filter making the opposite of another filter
 */
template<typename Scalar, class Filter> class FilterNot: public FilterBase<Scalar, FilterNot<Scalar, Filter>>
{
    public:
    /*! Default constructor
     * \param filter Apply not to this filter.
     */
    FilterNot(const Filter& filter):
        m_filter(filter)
    {
    }

    /*! Filter implementation
     */
    bool FilterImpl(const Eigen::Ref<const Eigen::Matrix<Scalar, 1, Eigen::Dynamic>>& m)
    {
        return !m_filter.FilterImpl(m);
    }

    private:
    Filter m_filter;
};

/*! Filter combining other filters and applying an and.
 */
template<typename Scalar, class... Tfilters> class FilterAnd: public FilterBase<Scalar, FilterAnd<Scalar, Tfilters...>>
{
    public:
    /*! Default constructor
     * \param filters List of filters to combine for And.
     */
    FilterAnd(Tfilters... filters): m_tuple(std::tuple<Tfilters...>(filters...)), m_size(std::tuple_size<std::tuple<Tfilters...>>::value)
    {
    }

    template<size_t N, typename... Tf> struct CompareAnd
    {
        bool operator()(std::tuple<Tf...>& ituple, const Eigen::Ref<const Eigen::Matrix<Scalar, 1, Eigen::Dynamic>>& m)
        {
            return (std::get<N>(ituple)).Filter(m) && CompareAnd<N-1, Tf...>{}(ituple, m);
        }
    };

    template<typename... Tf> struct CompareAnd<0, Tf...>
    {
        bool operator()(std::tuple<Tf...>& ituple, const Eigen::Ref<const Eigen::Matrix<Scalar, 1, Eigen::Dynamic>>& m)
        {
            return (std::get<0>(ituple)).Filter(m);
        }
    };

    bool FilterImpl(const Eigen::Ref<const Eigen::Matrix<Scalar, 1, Eigen::Dynamic>>& m)
    {
        bool result = CompareAnd<std::tuple_size<std::tuple<Tfilters...>>::value-1, Tfilters...>()(m_tuple, m);

        return result;
    }

    private:
    std::tuple<Tfilters...> m_tuple; ///< tuple of filters used in this and filter.
    const size_t m_size; ///< number of filters.
};

/*! Filter combining other filters and applying an or.
 */
template<typename Scalar, class... Tfilters> class FilterOr: public FilterBase<Scalar, FilterOr<Scalar, Tfilters...>>
{
    public:
    /*! Default constructor
     * \param filters List of filters to combine for Or.
     */
    FilterOr(Tfilters... filters): m_tuple(std::tuple<Tfilters...>(filters...)), m_size(std::tuple_size<std::tuple<Tfilters...>>::value)
    {
    }

    template<size_t N, typename... Tf> struct CompareOr
    {
        bool operator()(std::tuple<Tf...>& ituple, const Eigen::Ref<const Eigen::Matrix<Scalar, 1, Eigen::Dynamic>>& m)
        {
            return (std::get<N>(ituple)).Filter(m) || CompareOr<N-1, Tf...>{}(ituple, m);
        }
    };

    template<typename... Tf> struct CompareOr<0, Tf...>
    {
        bool operator()(std::tuple<Tf...>& ituple, const Eigen::Ref<const Eigen::Matrix<Scalar, 1, Eigen::Dynamic>>& m)
        {
            return (std::get<0>(ituple)).Filter(m);
        }
    };

    /*! Filter implementation
     */
    bool FilterImpl(const Eigen::Ref<const Eigen::Matrix<Scalar, 1, Eigen::Dynamic>>& m)
    {
        bool result = CompareOr<std::tuple_size<std::tuple<Tfilters...>>::value-1, Tfilters...>()(m_tuple, m);

        return result;
    }

    private:
    std::tuple<Tfilters...> m_tuple; ///< tuple of filters used in this and filter.
    const size_t m_size; ///< number of filters.
};

/*! Apply a filter to each column of a matrix.
 * \param matrix Apply filter to each column of the matrix.
 * \param filter Filter to apply.
 * \return Boolean array of size (matrix.cols())
 */
template<typename Scalar, class Filter> inline FilterArray Apply(const Eigen::Ref<const Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>>& matrix, Filter filter)
{
    FilterArray result(matrix.cols());
    for(int i = 0; i < matrix.cols(); ++i)
    {
        result(i) = filter.Filter(matrix.col(i));
    }

    return result;
}

/*! Helper function to make FilterAnd.
 * \param filters List of filters to build FilterAnd.
 */
template<typename Scalar, class... Tfilters> FilterAnd<Scalar, Tfilters...> MakeFilterAnd(Tfilters... filters)
{
    return FilterAnd<Scalar, Tfilters...>(std::forward<Tfilters>(filters)...);
}

/*! Helper function to make FilterOr.
 * \param filters List of filters to build FilterOr.
 */
template<typename Scalar, class... Tfilters> FilterOr<Scalar, Tfilters...> MakeFilterOr(Tfilters... filters)
{
    return FilterOr<Scalar, Tfilters...>(std::forward<Tfilters>(filters)...);
}

/*! Helper function to make FilterNot.
 * \param filter Filter to build FilterNot.
 */
template<typename Scalar, class Tfilter> FilterNot<Scalar, Tfilter> MakeFilterNot(Tfilter filter)
{
    return FilterNot<Scalar, Tfilter>(filter);
}

#endif
