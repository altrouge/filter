#ifndef _FULLDYNFILTER_H_
#define _FULLDYNFILTER_H_

#include "Eigen/Core"
#include "Eigen/Geometry"

#include "common.h"

class FullDynFilter
{
    public:
    virtual FilterArray Filter(const Eigen::Ref<const Eigen::MatrixXd>& m) = 0;
};

class FullDynSphereFilter: public FullDynFilter
{
    public:
    FullDynSphereFilter(double radius = 0): m_radius2(radius*radius)
    {
    }

    FilterArray Filter(const Eigen::Ref<const Eigen::MatrixXd>& m) override
    {
        FilterArray result(m.cols());
        for(int i = 0; i < m.cols(); ++i)
        {
            result(i) = (m(0,i)*m(0,i) + m(1,i)*m(1,i) + m(2,i)*m(2,i) < m_radius2);
        }
        return result;
        //return (m.colwise().squaredNorm().array() < m_radius2);
    }

    double m_radius2;
};
class FullDynAndFilter: public FullDynFilter
{
    public:
    FullDynAndFilter()
    {
    }

    void AddFilter(FullDynFilter* filter)
    {
        m_filters.push_back(filter);
    }

    FilterArray Filter(const Eigen::Ref<const Eigen::MatrixXd>& m) override
    {
        FilterArray result(m.cols());
        result.fill(true);
        for(auto& filter: m_filters)
        {
            FilterArray f = filter->Filter(m);
            for(int i = 0; i < result.size(); ++i)
            {
                result(i) = result(i) && f(i);
            }
        }

        return result;
    }

    private:
    std::vector<FullDynFilter*> m_filters;
};

#endif
