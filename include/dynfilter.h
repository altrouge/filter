#ifndef _DYNFILTER_H_
#define _DYNFILTER_H_

#include "Eigen/Core"
#include "Eigen/Geometry"

#include "common.h"

// Let us compare it with dynamic filter:
class DynFilter
{
    public:
    virtual bool Filter(const Eigen::Ref<const Eigen::VectorXd>& m) = 0;
};

class DynSphereFilter: public DynFilter
{
    public:
    DynSphereFilter(double radius = 0): m_radius2(radius*radius)
    {
    }

    bool Filter(const Eigen::Ref<const Eigen::VectorXd>& m) override
    {
        return m(0)*m(0) + m(1)*m(1) + m(2)*m(2) < m_radius2;
    }

    double m_radius2;
};

class DynAndFilter: public DynFilter
{
    public:
    DynAndFilter()
    {
    }

    void AddFilter(DynFilter* filter)
    {
        m_filters.push_back(filter);
    }

    bool Filter(const Eigen::Ref<const Eigen::VectorXd>& m) override
    {
        bool result = true;
        for(auto& filter: m_filters)
        {
            result = result && filter->Filter(m);
        }
        return result;
    }

    private:
    std::vector<DynFilter*> m_filters;
};

inline FilterArray DynApply(const Eigen::Ref<const Eigen::MatrixXd>& matrix, DynFilter* filter)
{
    FilterArray result(matrix.cols());
    for(int i = 0; i < matrix.cols(); ++i)
    {
        result(i) = filter->Filter(matrix.col(i));
    }

    return result;
}
#endif
